use git_url_parse::GitUrl;
use std::process::Command;

fn gen_path(url: GitUrl) -> String {
    let repos_path = match std::env::var("REPOS") {
        Ok(val) => val,
        Err(_) => panic!("$REPOS is not set"),
    };

    let host = match url.host {
        Some(host) => host,
        None => panic!("No host was found in the url"),
    };

    let url_path = if url.path.contains(".git") {
        String::from(&url.path[..url.path.len() - 4])
    } else {
        url.path
    };

    return format!("{repos_path}/{host}{url_path}");
}

fn clone(url: &String, args: Vec<String>, path: &String) {
    println!("Cloning {url} into: {path}");
    let mut cmd = Command::new("git");
    cmd.arg("clone");
    if !args.is_empty() {
        cmd.args(args);
    }
    cmd.args([url, path]);
    let _ = cmd.status().expect("Fail");
}

fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.len() <= 1 {
        panic!("None enougth values where provided");
    }

    let flags = &args[1..args.len() - 1];
    let url = args.last().unwrap();
    let parsed_url = GitUrl::parse(url.as_str()).unwrap();
    let path = gen_path(parsed_url);

    clone(&url, flags.to_vec(), &path);
}
